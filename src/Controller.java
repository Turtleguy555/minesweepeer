//David Yin P1 4/7
//This lab took me about 20 hours combined
//The main struggle with this lab was finding a way to connect the view
//model and the controller together
//However, I was able to solve this issue by creating an array of image views
//That allowed me to preciesley gather the place of each image view, as well
//as allowing me to modify image views at precise locations.


import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Controller extends Application{
    int counter = 0;
    int SIZE = 500;
    int numRow = 10;
    int numCol = 15;
    boolean firstClick = true;
    double width;
    double height;
    boolean isGameOver = false;
    boolean isGameWon = false;
    MenuBar m;
    Menu op;
    Menu he;
    Menu game;
    ImageView[][] imArray;
    Stage stage;
    BorderPane root;
    Scene scene;
    GridPane g;
    Label lab;
    P1_David_Yin_Console model;
    Image flag;
    Image zero;
    Image one;
    Image two;
    Image three;
    Image four;
    Image five;
    Image six;
    Image seven;
    Image eight;
    Image bomb;
    Image blank;
    Image revealed;
    HBox h;
    HBox v1;
    HBox v2;
    ImageView im;
    Label minesLeft;
    Label timer;
    long delay = 1;
    long lastUp;
    HBox modules;
    
    boolean sss = true;
    
    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage s) throws Exception {
        
        flag = new Image("bomb_flagged.gif");
        
        blank = new Image("blank.gif");
        
        bomb = new Image("bomb_revealed.gif");
        
        zero = new Image("num_0.gif");
        one = new Image("num_1.gif"); 
        two = new Image("num_2.gif");
        three = new Image("num_3.gif");
        four = new Image("num_4.gif");
        five = new Image("num_5.gif");
        six = new Image("num_6.gif");
        seven = new Image("num_7.gif");
        eight = new Image("num_8.gif");
        
        stage = s;
        stage.setTitle("Minesweeper");
        stage.setResizable(false);
        stage.sizeToScene();
        root = new BorderPane();    
        scene = new Scene(root, SIZE, SIZE);
        stage.setScene(scene);
        
        g = new GridPane();
        
        v1 = new HBox();
        width = ((SIZE/2) - (numCol*two.getHeight()/2))/2;
        v1.setPadding(new Insets(width));
        root.setLeft(v1);
        
        
        
        
        
        model = new P1_David_Yin_Console(numRow,numCol,20);
        
        
        
        MyClick myClick = new MyClick();
        root.addEventHandler(MouseEvent.MOUSE_CLICKED, myClick );

        imArray = new ImageView[numRow][numCol];
        Region reg1 = new Region();
        
    
        
        
        root.setCenter(g);
        
    
        m = new MenuBar();
        game = new Menu("Game"); 
        op = new Menu("Options"); 
        he = new Menu("Help");
        m.getMenus().addAll(game,op,he);
        VBox men = new VBox();
        men.getChildren().add(m);
        
        Beg b = new Beg();
        MenuItem Beginner = new MenuItem("Beginner, 6 x 6, 6 mines");
        Beginner.setOnAction(b);
        MenuItem Intermediate = new MenuItem("Intermediate, 10 x 10, 30 mines");
        MenuItem Advanced = new MenuItem("Adcanced, 15 x 15, 100 mines");
        game.getItems().addAll(Beginner, Intermediate, Advanced);
        root.setTop(men);
        
        
        
        
        
        
        
        
        
        modules = new HBox();
        men.getChildren().add(modules);
        
        minesLeft = new Label("Mines Left: " + model.nMines());
        modules.getChildren().add(minesLeft);
        v2 = new HBox();
        timer = new Label("     Time: 0");
        modules.getChildren().add(timer);
        
        height = (((SIZE/2) - m.getHeight() ) - (numRow*two.getHeight()/2))/2;
        v2.setPadding(new Insets(height));
        men.getChildren().add(v2);
        
        
        
        
        setView();
    
        
        
        
        
        
        
        
            
        
        
        
        
        
        
        stage.show();
        
        
        
        
        
        
    }
    
    class Beg implements EventHandler<ActionEvent>{


        @Override
        public void handle(ActionEvent e) {
            reset();
            
            
            
        }
    }
    
    void reset() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                ImageView iv = new ImageView(blank);
                g.add(iv, j, i);
                g.getChildren().remove(iv);
            }
        }
        
    }
    
    
    
    void setView() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                ImageView c = new ImageView();
                c.setImage(blank);
                g.add(c,j,i);
            }
        }
    }
    
    void setHiddenView() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if(model.isMine(i, j)) {
                    ImageView c = new ImageView();
                    c.setImage(bomb);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 0) {
                    ImageView c = new ImageView();
                    c.setImage(zero);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 1) {
                    ImageView c = new ImageView();
                    c.setImage(one);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 2) {
                    ImageView c = new ImageView();
                    c.setImage(two);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 3) {
                    ImageView c = new ImageView();
                    c.setImage(three);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 4) {
                    ImageView c = new ImageView();
                    c.setImage(four);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 5) {
                    ImageView c = new ImageView();
                    c.setImage(five);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 6) {
                    ImageView c = new ImageView();
                    c.setImage(six);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 7) {
                    ImageView c = new ImageView();
                    c.setImage(seven);
                    imArray[i][j] = c;
                    
                }else if(model.numMines(i, j) == 8) {
                    ImageView c = new ImageView();
                    c.setImage(eight);
                    imArray[i][j] = c;
                    
                }
                
                
            }
        }
    }
    
    void updateView() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if(model.isFlagged(i, j)) {
                    ImageView c = new ImageView();
                    c.setImage(flag);
                    
                    g.add(c, j, i);
                    
                }else if(model.isCovered(i, j)) {
                    try {
                        
                        
                        g.add(imArray[i][j],j,i);
                    }catch(IllegalArgumentException f) {
                        
                    }
                    
                }else if(!model.isCovered(i, j)) {
                    ImageView c = new ImageView();
                    c.setImage(blank);
                    
                    g.add(c, j, i);
                }
                
                    
                
                
            }
        }
    }
    
    
    
    
    
    class MyClick implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            try {
                isWon();
                int col = (int)((e.getX()-width*2)/blank.getHeight());
                int row = (int)((e.getY()- minesLeft.getHeight()- m.getHeight()-height*2)/blank.getHeight());
                if(firstClick == true && model.isBounds(row,col) && e.getButton() == MouseButton.PRIMARY) {
                    model.setModBoard(row,col);
                    
                    setHiddenView();
                    model.recursiveReveal(row,col);
                    updateView();
                    Anime a = new Anime();
                    a.start();
                    
                    firstClick = false;
                    
            
                    
                }else if(e.getButton() == MouseButton.SECONDARY && !model.isFlagged(row, col) && !model.isCovered(row, col) && isGameOver == false && isWon()==false) {
                    model.setFlag(row, col);
                    updateView();
                    minesLeft.setText("Mines Left: " + model.nMines());
                    
                
                }else if(e.getButton() == MouseButton.SECONDARY && model.isFlagged(row, col) && isGameOver == false && isWon()==false) {
                    model.removeFlag(row, col);
                    updateView();
                    minesLeft.setText("Mines Left: " + model.nMines());
                    
                }else if(e.getButton() == MouseButton.PRIMARY && isGameOver == false && isWon()==false && !model.isFlagged(row,col)) {
                    if(model.isMine(row, col)) {
                        Label l = new Label("Game Over");
                        root.setBottom(l);
                        isGameOver = true;
                        GameOver();
                    
                    
                    }else {
                        
                            
                            model.recursiveReveal(row,col);
                            updateView();
                        
                        
                    }
                }
            
            }catch(ArrayIndexOutOfBoundsException i){
                
            }
            
            firstClick = false;
                
                
                
                
    
            
        }
    }
    
    void GameOver() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if(model.isMine(i, j)) {
                    ImageView c = new ImageView();
                    c.setImage(bomb);
                    g.add(c , j, i);
                }
                
            }
        }
        
    }
    
    boolean isWon() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if(!model.isCovered(i,j) || !model.isFlagged(i, j)==  model.isMine(i, j)) {
                    isGameWon = false;
                    return isGameWon;
                    
                }
                
            }
        }
        isGameWon = true;
        return isGameWon;
    }
    
    
    class Anime extends AnimationTimer{

        @Override
        public void handle(long arg) {
            
            if((arg - lastUp) >= delay*1e9) {
                counter++;
                timer.setText("     Time: " + counter);
                lastUp = arg;
            
            }
            
            if(isWon() == true) {
                stop();
                Label lab = new Label("You Win");
                root.setBottom(lab);
            }else if(isGameOver == true) {
                stop();
                
                
            }
        
            
            
            

            
            
            
            
                
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    

}


