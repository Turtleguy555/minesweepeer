
import java.util.Random;

public class P1_David_Yin_Console{
    int row;
    int col;
    int totM;
    int tM;
    boolean isGameOver = false;
    String[][] board;
    String[][] dBoard;
    boolean[][] rBoard;
    boolean isWon = false;
    public P1_David_Yin_Console(int r, int c, int t) {
        row = r;
        col = c;
        totM = t;
        tM = t;
        board = new String[row][col];
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                board[i][j] = "_";
            }
        }
        dBoard = new String[row][col];
        rBoard = new boolean[row][col];
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                rBoard[i][j] = false;
            }
        }
        
        
    }
    
    public void setBoard() {
        
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                dBoard[i][j] = "0";
            }
        }
        
        while(true) {
            for(int i = 0; i < row; i++) {
                for(int j = 0; j < col; j++) {
                    Random r = new Random();
                    int rand = r.nextInt((row * col)+1);
                    if(rand == 0 && !dBoard[i][j].equals("*")) {
                        dBoard[i][j] = "*";
                        totM--;
                    }
                    if(totM == 0) {
                        break;
                    }
                    
                }
                if(totM == 0) {
                    break;
                }
            }
            
            if(totM == 0) {
                break;
            }
            
        }
        detNums();
    }
    
    public void printUp() {
        int counter = 0;
        System.out.print("   ");
        for(int i = 0; i < row; i++) {
            if(counter < 10) {
                System.out.print(counter+" ");
                counter++;
            }else {
                counter++;
                counter = 0;
            }
            
            
        }
        //System.out.println("");
        System.out.println("");
        //int counter = 0;
        for(int i = 0; i < row; i++) {
            if(i < 10) {
                System.out.print(" " + (i) + " ");
            }else {
                System.out.print((i)+ "  ");
            }
            
            for(int j = 0; j < col; j++) {
                //System.out.print();
                
                System.out.print(board[i][j]+" ");
                
            }
            System.out.println();
        }
        
        
        
    }
    
    public int numMines(int r, int c) {
        int counter = 0;
        
        for(int i = r-1; i <= r+1; i++) {
            for(int j = c-1; j <= c+1;j++) {
                if(isBounds(i,j)) {
                    if(dBoard[i][j].equals("*")) {
                        if(i != r || j != c) {
                            
                            counter++;
                        }
                    }
                }
            }
        }
        
        return counter;
    }
    
    
    public boolean isBounds(int i,int j) {
        if((i >=0 && i < row) && (j >=0 && j < col)) {
            //System.out.println("true");
            
            return true;
        }
        return false;
    }
    
    public void printDown() {
        int counter = 0;
        System.out.println();
        System.out.print("   ");
        for(int i = 0; i < row; i++) {
            if(counter < 10) {
                System.out.print(counter+" ");
                counter++;
            }else {
                counter++;
                counter = 0;
            }
            
            
        }
        //System.out.println("");
        System.out.println("");
        //int counter = 0;
        for(int i = 0; i < row; i++) {
            if(i < 10) {
                System.out.print(" " + (i) + " ");
            }else {
                System.out.print((i)+ "  ");
            }
            
            for(int j = 0; j < col; j++) {
                //System.out.print();
                
                System.out.print(dBoard[i][j] + " ");
                
            }
            System.out.println();
        }
        
    }
    
    
    public void detNums() {
        int num;
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                if(!dBoard[i][j].equals("*")) {
                    num = numMines(i,j);
                    dBoard[i][j] = "" + num;
                    
                }
                
                
            }
        }
    }
    
    public void setCell(int r, int c, String s) {
        if(s.equals("r")) {
            
            if(dBoard[r][c].equals("*")) {
                System.out.println("Game Over!");
                board[r][c] = dBoard[r][c];
                isGameOver = true;
                endGame();
            }else {
                recursiveReveal(r,c);
            }
            
            
            
        }else if(s.equals("f")){
            board[r][c] = "f";
            tM--;
            
        }
        
        printUp();
        printDown();
    }
    
    public int nMines() {
        return tM;
        
    }
    
    public boolean endGame() {
        
        return isGameOver;
    }
    
    public void recursiveReveal(int r, int c) {
        if(isBounds(r,c)) {
            
            if(board[r][c].equals("_") && dBoard[r][c].equals("0") && isBounds(r,c)) { 
                board[r][c] = dBoard[r][c];
                
        
                recursiveReveal(r-1,c);
                
                recursiveReveal(r+1,c);
                recursiveReveal(r-1,c-1);
                recursiveReveal(r,c-1);
                recursiveReveal(r+1,c-1);
                recursiveReveal(r-1,c+1);
                recursiveReveal(r,c+1);
                recursiveReveal(r+1,c+1);
            
            }else if(board[r][c].equals("_")) {
                board[r][c] = dBoard[r][c];
            
            }
        }

    }
    
    public boolean isWon() {
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                if(board[i][j].equals("_")) {
                    return false;
                }
            }
        }
        return true;
        
    }
    
    public int getNumRows() {
        return row;
    }
    
    public int getNumCols() {
        return col;
    }
    
    public boolean isCovered(int r, int c) {
        if(board[r][c].equals("_")) {
            return false;
        }
        return true;
    }
    
    public boolean isFlagged(int r, int c) {
        if(board[r][c].equals("f")) {
            return true;
        }
        return false;
    }
    
    public boolean isMine(int r, int c) {
        if(dBoard[r][c].equals("*")) {
            return true;
        }
        return false;
    }
    
    public void setFlag(int r, int c) {
        board[r][c] = "f";
        tM--;
    }
    
    public void removeFlag(int r, int c) {
        
            board[r][c] = "_";
            tM++;
        
    }
    
    public void setModBoard(int r , int c) {
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                dBoard[i][j] = "0";
            }
        }
        
        while(true) {
            for(int i = 0; i < row; i++) {
                for(int j = 0; j < col; j++) {
                    Random r1 = new Random();
                    int rand = r1.nextInt((row * col)+1);
                    if(rand == 0 && !dBoard[i][j].equals("*")) {
                        if((i != r && j != c)) {
                            dBoard[i][j] = "*";
                            totM--;
                            
                        }
                        
                    }
                    if(totM == 0) {
                        break;
                    }
                    
                }
                if(totM == 0) {
                    break;
                }
            }
            
            if(totM == 0) {
                break;
            }
            
        }
        detNums();
        
    }
    
    
    public void setDefBoard() {
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                board[i][j] = "0";
                dBoard[i][j] = "0";
            }
        }
            
        
    }
    
    
    
    
    
}

















